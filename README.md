[![pipeline status](https://gitlab.com/advprog-group-9/card-backend/badges/master/pipeline.svg)](https://gitlab.com/advprog-group-9/card-backend/-/commits/master)
[![coverage report](https://gitlab.com/advprog-group-9/card-backend/badges/master/coverage.svg)](https://gitlab.com/advprog-group-9/card-backend/-/commits/master)
# Yugi-Eh

## Penjelasan Game
Pada tugas akhir ini kami akan membuat suatu game multiplayer online yang dapat dimainkan melalui web browser. Game ini dimainkan dengan cara 2 player bertarung menggunakan kartu. Kartu - kartu ini akan menambah status player (HP, strength, armor). Player dapat menggunakan maksimal 3 kartu disetiap ronde nya. Player dapat menyerang player lain dan akan menang jika player lawan kehabisan HP.


## Design Patterns
### Observer Pattern
Dalam game kami, akan terdapat dua komponen yaitu `Room` dan `Players`. Design principle yang kami ikuti adalah `Hollywood Principle`, karena project kami adalah sebuah game online dimana player memerlukan data dari room untuk mengecek kondisi dia dan lawan. Kami tidak ingin sang player untuk menanyakan kondisi room secara terus-menerus, melainkan room nanti yang akan memberitahukan player aksi apa yang sedang terjadi, dan player akan diupdate terkait aksi tersebut. 

### Factory Method
Dalam game kami, terdapat berbagai jenis kartu kartu, seperti kartu attack, kartu defense, dan kartu heal. Kami ingin menerapkan  design principle `Implementation Over Inheritance`, sehingga kami akan membuat suatu interface kartu yang akan di implementasikan oleh masing-masing class kartu. Kami akan membuat suatu class factory yang akan membuat object kartu yang sesuai dengan jenis kartu yang diinginkan.