package com.advprog.stratcard.players.core;

import com.advprog.stratcard.cards.core.AttackBuffCard;
import com.advprog.stratcard.cards.core.AttackCard;
import com.advprog.stratcard.cards.core.Card;
import com.advprog.stratcard.cards.core.HealCard;
import com.advprog.stratcard.room.core.Room;
import com.advprog.stratcard.room.repository.RoomRepository;
import com.advprog.stratcard.room.service.RoomService;
import com.advprog.stratcard.room.service.RoomServiceImpl;
import com.advprog.stratcard.roomtokens.core.Token;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;

class PlayerTest {
    Player playerOne;
    Player playerTwo;
    RoomRepository roomRepository = new RoomRepository();
    RoomService roomService = new RoomServiceImpl(roomRepository);

    @BeforeEach
    public void setUp() {
        playerOne = new Player("Player1");
        playerTwo = new Player("Player2");
    }

    @Test
    public void testSetRoom() {
        Room room = new Room(new Token("A"));
        playerOne.setRoom(room);
        playerTwo.setRoom(room);
        assertEquals(playerOne.getRoom(), playerTwo.getRoom());
    }

    @Test
    public void testUseCard() {
        Room room = new Room(new Token("A"));
        playerOne.setRoom(room);
        playerTwo.setRoom(room);
        Card card = new HealCard("heal25", "Heal user by 25 HP", playerOne, "Heal");

        int expectedHp = 275;

        playerOne.selectCard(card);
        playerOne.useSelectedCard(playerTwo);
        assertEquals(playerOne.getHp(), expectedHp);

        int expectedOpponentHp = 240;

        card = new AttackCard("Dagger", "Do damage to enemy = atk", playerOne, "Attack");
        card.setTarget(playerTwo);
        playerOne.selectCard(card);
        playerOne.useSelectedCard(playerTwo);
        assertEquals(playerTwo.getHp(), expectedOpponentHp);
    }

    @Test
    public void testAddAtk() {
        int expectedAtk = 20;
        int atkIncrease = 10;
        playerOne.addAtk(atkIncrease);

        int playerOneAtk = playerOne.getAtk();

        assertEquals(playerOneAtk, expectedAtk);
    }

    @Test
    public void testGetUsername() {
        String expectedUsername = "Player1";
        String playerOneUsername = playerOne.getUsername();
        assertEquals(playerOneUsername, expectedUsername);
    }

    @Test
    public void testDeck() {
        Card[] smallDeck = roomService.randomAndGetFiveCards();
        playerOne.setDeck(smallDeck);

        Card[] playerOneDeck = playerOne.getDeck();
        assertEquals(playerOneDeck, smallDeck);
    }

    @Test
    public void testActivateCard() {
        Card healCard = new HealCard("heal25", "Heal user by 25 HP", playerOne, "Heal");
        Card[] deck = {healCard};
        playerOne.setDeck(deck);

        int initialHp = playerOne.getHp();
        playerOne.activateSelectedCard();
        assertEquals(initialHp + 25, playerOne.getHp());

        Card attackCard = new AttackBuffCard("Dagger", "Increase ATK by 15", playerOne, "AttackBuff");
        Card[] newDeck = {healCard, attackCard};
        playerOne.setDeck(newDeck);

        int newInitialHp = playerOne.getHp();
        int initialDmg = playerOne.getAtk();
        String[] indexes = {"eh", "0", "1"};
        playerOne.activateSelectedCard(indexes);
        assertEquals(newInitialHp + 25, playerOne.getHp());
        assertEquals(initialDmg + 15, playerOne.getAtk());
    }

    @Test
    public void testGetUsedCard() {
        Card healCard = new HealCard("heal25", "Heal user by 25 HP", playerOne, "Heal");
        Card attackCard = new AttackBuffCard("Dagger", "Increase ATK by 15", playerOne, "AttackBuff");
        Card[] deckCards = {healCard, attackCard};

        playerOne.setDeck(deckCards);
        playerOne.activateSelectedCard();

        String[] indexes = {"roomtoken", "0", "1"};
        Card[] usedCards = playerOne.getCardsUsed(indexes);
        assertEquals(deckCards[0].getName(), usedCards[0].getName());
        assertEquals(deckCards[1].getName(), usedCards[1].getName());
    }

    
}
