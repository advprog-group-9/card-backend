package com.advprog.stratcard.players.repository;

import com.advprog.stratcard.players.core.Player;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import java.util.Map;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class PlayerRepositoryTest {
    PlayerRepository playerRepository;
    Map<Long, Player> playersInRepository;
    Player player;

    @BeforeEach
    public void setUp() {
        playerRepository = new PlayerRepository();
        playersInRepository = playerRepository.players;
        player = new Player("Test");
    }

    @Test
    public void putPlayerShouldReturnTheSamePlayerPut() {
        assertEquals(playerRepository.putPlayer(0L, player), player);
    }

    @Test
    public void putPlayerShouldAddPlayerToTheMap() {
        playerRepository.putPlayer(0L, player);
        assertEquals(playersInRepository.size(), 1);
    }

    @Test
    public void getPlayerShouldReturnTheCorrespondingId() {
        playersInRepository.put(0L, player);
        assertEquals(playerRepository.getPlayer(0L), player);
    }
}
