package com.advprog.stratcard.room.repository;

import com.advprog.stratcard.room.core.Room;
import com.advprog.stratcard.roomtokens.core.Token;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class RoomRepositoryTests {
    RoomRepository roomRepository = new RoomRepository();
    Token token;
    Room room;

    @BeforeEach
    public void setUp() {
        token = new Token("AAAAA");
        room = new Room(token);
    }

    @Test
    public void testsPutRoomShouldReturnTheSameRoomIfValid() {
        Room returnedRoom = roomRepository.putRoom(token, room);
        assertEquals(returnedRoom, room);
    }

    @Test
    public void testsGetRoomShouldReturnRoomWithAssosiatedToken() {
        roomRepository.putRoom(token, room);
        assertEquals(room, roomRepository.getRoom(token.getTokenId()));
    }
}
