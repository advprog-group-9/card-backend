package com.advprog.stratcard.room.service;

import com.advprog.stratcard.cards.core.Card;
import com.advprog.stratcard.room.core.Room;
import com.advprog.stratcard.room.repository.RoomRepository;
import com.advprog.stratcard.roomtokens.core.Token;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class RoomServiceImplTest {
    RoomRepository roomRepository = new RoomRepository();
    RoomServiceImpl roomService = new RoomServiceImpl(roomRepository);
    Token token;
    Room room;

    @Test
    public void testCreateRoom() {
        Object returnedObject = roomService.createRoom();
        assertTrue(returnedObject instanceof Room);
    }

    @Test
    public void testGetRoomWithToken() {
        token = new Token("AAAAA");
        room = new Room(token);
        roomRepository.putRoom(token, room);
        Room returnedObject = roomService.getRoomWithToken(token.getTokenId());
        assertTrue(returnedObject instanceof Room);
        assertEquals(token.getTokenId(), returnedObject.getToken().getTokenId());
    }

    @Test
    public void testRandomAndGetFiveCards() {
        Card[] setOfFiveCards = roomService.randomAndGetFiveCards();
        int numberOfCards = setOfFiveCards.length;
        assertEquals(numberOfCards, 5);
    }
}
