package com.advprog.stratcard.room.core;

import com.advprog.stratcard.players.core.Player;
import com.advprog.stratcard.roomtokens.core.Token;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class RoomTest {
    Room room = new Room(new Token("AAAAA"));

    @Test
    public void testGetRoomToken() {
        Token roomToken = room.getToken();
        String expectedTokenId = "AAAAA";
        String roomTokenId = roomToken.getTokenId();
        assertEquals(roomTokenId, expectedTokenId);
    }

    @Test
    public void testSetAndGetPlayersOneAndTwo() {
        Player mockPlayerOne = new Player("Tester Number One");
        mockPlayerOne.setRoom(room);

        Player mockPlayerTwo = new Player("Tester Number Two");
        mockPlayerTwo.setRoom(room);

        room.setPlayerOne(mockPlayerOne);
        room.setPlayerTwo(mockPlayerTwo);

        Player returnedPlayerOneFromRoom = room.getPlayerOne();
        assertEquals(returnedPlayerOneFromRoom, mockPlayerOne);
        assertTrue(room.isPlayerOne(returnedPlayerOneFromRoom));

        Player returnedPlayerTwoFromRoom = room.getPlayerTwo();
        assertEquals(returnedPlayerTwoFromRoom, mockPlayerTwo);
    }

    @Test
    public void testStartGame() {
        room.startGame();
        assertTrue(room.getState() instanceof RoomPlayerOneSelectState);
    }

    @Test
    public void testSetState() {
        RoomState state = new RoomPlayerOneSelectState(room);
        room.setState(state);
    }
}
