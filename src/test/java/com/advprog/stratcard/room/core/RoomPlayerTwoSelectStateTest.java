package com.advprog.stratcard.room.core;

import com.advprog.stratcard.cards.core.Card;
import com.advprog.stratcard.cards.core.HealCard;
import com.advprog.stratcard.exception.MethodIsNotSupportedDuringThisStateException;
import com.advprog.stratcard.players.core.Player;
import com.advprog.stratcard.roomtokens.core.Token;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertTrue;

class RoomPlayerTwoSelectStateTest {
    Room room = new Room(new Token("AAAAA"));
    RoomState currentState = new RoomPlayerTwoSelectState(room);
    Player playerOne = new Player("MockTesterOne");
    Player playerTwo = new Player("MockTesterTwo");
    Card mockCard = new HealCard("Heal50", "Heal user by 50 HP", playerOne, "Heal");

    @Test
    public void testPlayerTwoSelectAndUse() {
        room.setPlayerOne(playerOne);
        room.setPlayerTwo(playerTwo);
        room.setState(currentState);
        room.playerTwoSelectAndUse(mockCard);
        assertTrue(room.getState() instanceof RoomPlayerOneSelectState);
    }

    @Test
    public void testPlayerOneSelectAndUse() {
        room.setPlayerOne(playerOne);
        room.setPlayerTwo(playerTwo);
        room.setState(currentState);

        try {
            room.playerOneSelectAndUse(mockCard);

        } catch (MethodIsNotSupportedDuringThisStateException exception) {
            assertTrue(exception instanceof MethodIsNotSupportedDuringThisStateException);
        }
    }
}
