package com.advprog.stratcard.room.core;

import com.advprog.stratcard.cards.core.AttackCard;
import com.advprog.stratcard.exception.MethodIsNotSupportedDuringThisStateException;
import com.advprog.stratcard.roomtokens.core.Token;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class RoomWaitingStateTest {
    RoomWaitingState roomWaitingState;
    Room room;
    AttackCard card;

    @BeforeEach
    public void setUp() {
        room = new Room(new Token("A"));
        roomWaitingState = new RoomWaitingState(room);
        card = new AttackCard("A", "A", null, "Attack");
    }

    @Test
    public void itShouldRaiseMethodIsNotSupportedExceptionIfPlayerOneSelectAndUseMethodIsCalled() {
        assertThrows(MethodIsNotSupportedDuringThisStateException.class, () -> {
            roomWaitingState.playerOneSelectAndUse(card);
        });
    }

    @Test
    public void itShouldRaiseMethodIsNotSupportedExceptionIfPlayerTwoSelectAndUseMethodIsCalled() {
        assertThrows(MethodIsNotSupportedDuringThisStateException.class, () -> {
            roomWaitingState.playerTwoSelectAndUse(card);
        });
    }
}
