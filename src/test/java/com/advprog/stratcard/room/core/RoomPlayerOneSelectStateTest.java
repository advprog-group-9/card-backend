package com.advprog.stratcard.room.core;

import com.advprog.stratcard.cards.core.Card;
import com.advprog.stratcard.cards.core.HealCard;
import com.advprog.stratcard.exception.MethodIsNotSupportedDuringThisStateException;
import com.advprog.stratcard.players.core.Player;
import com.advprog.stratcard.roomtokens.core.Token;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertTrue;

class RoomPlayerOneSelectStateTest {
    Room room = new Room(new Token("AAAAA"));
    RoomState currentState = new RoomPlayerOneSelectState(room);
    Player playerOne = new Player("MockTesterOne");
    Player playerTwo = new Player("MockTesterTwo");
    Card mockCard = new HealCard("heal10", "Heal user by 50 HP", playerOne, "Heal");


    @Test
    public void testPlayerOneSelectAndUse() {
        room.setPlayerOne(playerOne);
        room.setPlayerTwo(playerTwo);
        room.setState(currentState);
        room.playerOneSelectAndUse(mockCard);
        assertTrue(room.getState() instanceof RoomPlayerTwoSelectState);
    }

    @Test
    public void testPlayerTwoSelectAndUse() {
        room.setPlayerOne(playerOne);
        room.setPlayerTwo(playerTwo);
        room.setState(currentState);
        try {
            room.playerTwoSelectAndUse(mockCard);

        } catch (MethodIsNotSupportedDuringThisStateException exception) {
            assertTrue(exception instanceof MethodIsNotSupportedDuringThisStateException);
        }
    }
}
