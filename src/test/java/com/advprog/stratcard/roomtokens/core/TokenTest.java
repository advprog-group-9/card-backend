package com.advprog.stratcard.roomtokens.core;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

class TokenTest {
    Token token;
    TokenGeneratorInterface tokenGenerator = new TokenGenerator();

    @BeforeEach
    public void setUp() {
        token = tokenGenerator.generateToken();
    }

    @Test
    public void testSetTokenId() {
        token.setTokenId("ABC12");
        assertEquals(token.getTokenId(), "ABC12");
    }

    @Test
    public void testGetTokenId() {
        String tokenId = token.getTokenId();
        assertTrue(tokenId instanceof String);
    }

    @Test
    public void testIsValid() {
        assertTrue(token.isValid());
    }
}
