package com.advprog.stratcard.roomtokens.core;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;

class TokenGeneratorTest {
    TokenGeneratorInterface generator = new TokenGenerator();

    @Test
    public void testGenerateDigitAscii() {
        assertTrue(48 <= generator.generateDigitAscii());
        assertTrue(57 >= generator.generateDigitAscii());
    }

    @Test
    public void testGenerateCharAscii() {
        assertTrue(65 <= generator.generateCharAscii());
        assertTrue(90 >= generator.generateCharAscii());
    }

    @Test
    public void testGenerateToken() {
        Token generatedToken = generator.generateToken();
        String tokenId = generatedToken.getTokenId();
        assertEquals(tokenId.length(), 5);
    }
}
