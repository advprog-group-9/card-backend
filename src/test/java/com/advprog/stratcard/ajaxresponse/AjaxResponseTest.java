package com.advprog.stratcard.ajaxresponse;

import com.advprog.stratcard.cards.core.Card;
import com.advprog.stratcard.players.core.Player;
import com.advprog.stratcard.room.core.Room;
import com.advprog.stratcard.roomtokens.core.Token;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class AjaxResponseTest {
    Room room = new Room(new Token("AAAAA"));
    Player mockPlayerOne = new Player("Tester Number One");
    Player mockPlayerTwo = new Player("Tester Number Two");

    @BeforeEach
    public void setUp() {
        room.setPlayerOne(mockPlayerOne);
        room.setPlayerTwo(mockPlayerTwo);

    }
    
    @Test
    public void testType() {
        AjaxResponse ajaxResponse = new AjaxResponse(room);
        ajaxResponse.setType("Attack");
        assertEquals("Attack", ajaxResponse.getType());
    }

    @Test
    public void testDeck() {
        AjaxResponse ajaxResponse = new AjaxResponse(room);
        Card[] deck = new Card[1];
        ajaxResponse.setDeck(deck);
        assertEquals(deck, ajaxResponse.getDeck());
    }

    @Test
    public void testPlayerOneCardUsed() {
        AjaxResponse ajaxResponse = new AjaxResponse(room);
        Card[] deck = new Card[1];
        ajaxResponse.setPlayerOneCardUsed(deck);
        assertEquals(deck, ajaxResponse.getPlayerOneCardUsed());
    }

    @Test
    public void testPlayerTwoCardUsed() {
        AjaxResponse ajaxResponse = new AjaxResponse(room);
        Card[] deck = new Card[1];
        ajaxResponse.setPlayerTwoCardUsed(deck);
        assertEquals(deck, ajaxResponse.getPlayerTwoCardUsed());
    }
    
    @Test
    public void testPlayerStats() {
        AjaxResponse ajaxResponse = new AjaxResponse(room);
        assertEquals(mockPlayerOne, ajaxResponse.getPlayerOne());
        assertEquals(mockPlayerTwo, ajaxResponse.getPlayerTwo());
    }

}
