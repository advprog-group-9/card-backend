package com.advprog.stratcard.cards.repository;

import com.advprog.stratcard.cards.core.Card;
import com.advprog.stratcard.cards.core.HealCard;
import com.advprog.stratcard.players.core.Player;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

public class CardRepositoryTest {
    CardRepository cardRepository;
    Card healCard;

    @BeforeEach
    public void setUp() {
        cardRepository = new CardRepository();
        healCard = new HealCard("heal10", "Heal user by 10 HP", null, "HealCard");
    }

    @Test
    public void testPutCardToRepository() {
        this.cardRepository.putCard(12345, healCard);
        assertEquals(1, cardRepository.countNumOfCard());
        assertNotNull(cardRepository.getCard(12345));
    }

    @Test
    public void testRandomAndGetFiveCards() {
        cardRepository.createCards();

        int expectedSize = 5;
        Card[] arrayOfFiveCards = cardRepository.randomAndGetFiveCards();
        int sizeOfArray = arrayOfFiveCards.length;
        assertEquals(expectedSize, sizeOfArray);
    }

    @Test
    public void testCopyCard() {
        Player mockPlayerOne = new Player("PlayerOne");
        Player mockPlayerTwo = new Player("PlayerTwo");
        Card copiedCard = cardRepository.copyCard(healCard, mockPlayerOne, mockPlayerTwo);
    }
}
