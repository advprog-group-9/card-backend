package com.advprog.stratcard.cards.core;

import com.advprog.stratcard.players.core.Player;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;

class CardTest {
    Card healCard;
    Card attackBuffCard;
    Card attackCard;
    Player testPlayerActive;
    Player testPlayerPassive;

    @BeforeEach
    public void setUp() {
        testPlayerActive = new Player("Attacker");
        testPlayerPassive = new Player("Reciever");
        healCard = new HealCard("heal50", "Heal user by 50 HP", testPlayerActive, "Heal");
        attackBuffCard =  new AttackBuffCard("attackBuff25", "Increase ATK by 25", testPlayerActive, "AttackBuff");
        attackCard = new AttackCard("attack50", "Do damage to enemy = 2 * atk", testPlayerActive, "Attack");
        attackCard.setTarget(testPlayerPassive);
    }

    @Test
    public void testHealEffect() {
        int expectedHp = testPlayerActive.getHp() + 50;
        testPlayerActive.selectCard(healCard);
        testPlayerActive.useSelectedCard(testPlayerPassive);
        assertEquals(expectedHp, testPlayerActive.getHp());
    }

    @Test
    public void testAttackBuffEffect() {
        int expectedAtk = testPlayerActive.getAtk() + 25;
        testPlayerActive.selectCard(attackBuffCard);
        testPlayerActive.useSelectedCard(testPlayerPassive);
        assertEquals(expectedAtk, testPlayerActive.getAtk());
    }

    @Test
    public void testAttackEffect() {
        int expectedHp = testPlayerPassive.getHp() - 20;
        testPlayerActive.selectCard(attackCard);
        testPlayerActive.useSelectedCard(testPlayerPassive);
        assertEquals(expectedHp, testPlayerPassive.getHp());
    }

    @Test
    public void testGetCardName() {
        String expectedName = "heal50";
        assertEquals(expectedName, healCard.getName());
    }

    @Test
    public void testGetCardEffect() {
        String expectedEffect = "Heal user by 50 HP";
        assertEquals(expectedEffect, healCard.getEffect());
    }
}
