package com.advprog.stratcard.cards.core;

import com.advprog.stratcard.players.core.Player;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;

class HealCardTest {

    @Test
    public void activateEffect() {
        CardFactory factory = new CardFactory();

        Player owner = new Player("PlayerOne");
        String effect = "Heal user by 25 HP";
        String cardType = "HealCard";
        String cardName = "HEAL";

        int expectedHp = 275;

        Card testCard = factory.createCard(cardType, cardName, effect);
        testCard.setOwner(owner);
        testCard.activateEffect();
        assertEquals(expectedHp, owner.getHp());

        expectedHp = 325;

        effect = "Heal user by 50 HP";
        testCard = factory.createCard(cardType, cardName, effect);
        testCard.setOwner(owner);
        testCard.activateEffect();
        assertEquals(expectedHp, owner.getHp());

        expectedHp = 425;

        effect = "Heal user by 100 HP";
        testCard = factory.createCard(cardType, cardName, effect);
        testCard.setOwner(owner);
        testCard.activateEffect();
        assertEquals(expectedHp, owner.getHp());
    }
}
