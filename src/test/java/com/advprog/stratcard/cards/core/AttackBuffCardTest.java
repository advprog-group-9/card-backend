package com.advprog.stratcard.cards.core;

import com.advprog.stratcard.players.core.Player;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;

class AttackBuffCardTest {

    @Test
    public void activateEffect() {
        CardFactory factory = new CardFactory();

        Player owner = new Player("PlayerOne");
        String effect = "Increase ATK by 5";
        String cardType = "AttackBuffCard";
        String cardName = "BUFF";

        int expectedAtk = 15;

        Card testCard = factory.createCard(cardType, cardName, effect);
        System.out.println(testCard);
        testCard.setOwner(owner);
        testCard.activateEffect();
        assertEquals(expectedAtk, owner.getAtk());

        expectedAtk = 40;

        effect = "Increase ATK by 25";
        testCard = factory.createCard(cardType, cardName, effect);
        testCard.setOwner(owner);
        testCard.activateEffect();
        assertEquals(expectedAtk, owner.getAtk());

        expectedAtk = 55;

        effect = "Increase ATK by 15";
        testCard = factory.createCard(cardType, cardName, effect);
        testCard.setOwner(owner);
        testCard.activateEffect();
        assertEquals(expectedAtk, owner.getAtk());
    }
}
