package com.advprog.stratcard.cards.core;

import com.advprog.stratcard.players.core.Player;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;

class CardFactoryTest {
    CardFactory cardFactory = new CardFactory();

    @Test
    public void testCreateCard() {
        String cardType = "HealCard";
        String cardName = "Test Card";
        String effect = "Heal user by 25 HP";

        Card createdCard = cardFactory.createCard(cardType, cardName, effect);

        String createdCardName = createdCard.getName();
        String createdCardType = createdCard.getType();
        String createdCardEffect = createdCard.getEffect();
        assertEquals(cardName, createdCardName);
        assertEquals(cardType, createdCardType);
        assertEquals(effect, createdCardEffect);

        cardType = "AttackBuffCard";
        createdCard = cardFactory.createCard(cardType, cardName, effect);
        createdCardName = createdCard.getName();
        createdCardType = createdCard.getType();
        createdCardEffect = createdCard.getEffect();
        assertEquals(cardName, createdCardName);
        assertEquals(cardType, createdCardType);
        assertEquals(effect, createdCardEffect);

        cardType = "AttackCard";
        createdCard = cardFactory.createCard(cardType, cardName, effect);
        createdCardName = createdCard.getName();
        createdCardType = createdCard.getType();
        createdCardEffect = createdCard.getEffect();
        assertEquals(cardName, createdCardName);
        assertEquals(cardType, createdCardType);
        assertEquals(effect, createdCardEffect);
    }

    @Test
    public void testCopyAndCreateCard() {
        Player mockPlayerOne = new Player("PlayerOne");
        Player mockPlayerTwo = new Player("PlayerTwo");

        String cardType = "HealCard";
        String cardName = "Test Card";
        String effect = "Heal user by 25 HP";

        Card createdCard = cardFactory.createCard(cardType, cardName, effect);
        Card baseCard = cardFactory.createCard(createdCard, mockPlayerOne, mockPlayerTwo);

        String createdCardName = baseCard.getName();
        String createdCardType = baseCard.getType();
        String createdCardEffect = baseCard.getEffect();
        assertEquals(cardName, createdCardName);
        assertEquals(cardType, createdCardType);
        assertEquals(effect, createdCardEffect);

        cardType = "AttackBuffCard";
        createdCard = cardFactory.createCard(cardType, cardName, effect);
        baseCard = cardFactory.createCard(createdCard, mockPlayerOne, mockPlayerTwo);
        createdCardName = baseCard.getName();
        createdCardType = baseCard.getType();
        createdCardEffect = baseCard.getEffect();
        assertEquals(cardName, createdCardName);
        assertEquals(cardType, createdCardType);
        assertEquals(effect, createdCardEffect);

        cardType = "AttackCard";
        createdCard = cardFactory.createCard(cardType, cardName, effect);
        baseCard = cardFactory.createCard(createdCard, mockPlayerOne, mockPlayerTwo);
        createdCardName = baseCard.getName();
        createdCardType = baseCard.getType();
        createdCardEffect = baseCard.getEffect();
        assertEquals(cardName, createdCardName);
        assertEquals(cardType, createdCardType);
        assertEquals(effect, createdCardEffect);
    }
}
