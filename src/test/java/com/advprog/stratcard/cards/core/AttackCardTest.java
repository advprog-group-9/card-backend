package com.advprog.stratcard.cards.core;

import com.advprog.stratcard.players.core.Player;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;

class AttackCardTest {

    @Test
    public void activateEffect() {
        CardFactory factory = new CardFactory();

        Player owner = new Player("PlayerOne");
        Player target = new Player("PlayerTwo");
        String effect = "Do damage to enemy = atk";
        String cardType = "AttackCard";
        String cardName = "ATTACK";

        int expectedHp = 240;

        Card testCard = factory.createCard(cardType, cardName, effect);
        testCard.setOwner(owner);
        testCard.setTarget(target);
        testCard.activateEffect();
        assertEquals(expectedHp, target.getHp());

        expectedHp = 220;

        effect = "Do damage to enemy = 2 * atk";
        testCard = factory.createCard(cardType, cardName, effect);
        testCard.setOwner(owner);
        testCard.setTarget(target);
        testCard.activateEffect();
        assertEquals(expectedHp, target.getHp());

        expectedHp = 190;

        effect = "Do damage to enemy = 3 * atk";
        testCard = factory.createCard(cardType, cardName, effect);
        testCard.setOwner(owner);
        testCard.setTarget(target);
        testCard.activateEffect();
        assertEquals(expectedHp, target.getHp());
    }
}
