package com.advprog.stratcard.cards.core;

import com.advprog.stratcard.players.core.Player;

public abstract class Card {
    Player owner;
    Player target;
    String cardName;
    String effect;
    String type;

    public Card(String cardName, String effect, Player owner, String type) {
        this.cardName = cardName;
        this.effect = effect;
        this.owner = owner;
        this.type = type;
    }

    public Card(Card thatCard) {
        this.cardName = thatCard.cardName;
        this.effect = thatCard.effect;
        this.owner = thatCard.owner;
        this.type = thatCard.type;
    }

    public String getName() {
        return cardName;
    }

    public String getEffect() {
        return effect;
    }

    public String getType() {
        return type;
    }

    public void setOwner(Player player) {
        owner = player;
    }

    public void setTarget(Player player) {
        target = player;
    }

    public abstract void activateEffect();

    public String toString() {
        return "type :" + type + " name :" + cardName + " effect:" + effect;
    }
}
