package com.advprog.stratcard.cards.core;

import com.advprog.stratcard.players.core.Player;

public class CardFactory {

    public Card createCard(String cardType, String cardName, String effect) {
        Card card = null;
        if (cardType.equals("AttackCard")) {
            card = new AttackCard(cardName, effect, null, cardType);
        } else if (cardType.equals("HealCard")) {
            card = new HealCard(cardName, effect, null, cardType);
        } else if (cardType.equals("AttackBuffCard")) {
            card = new AttackBuffCard(cardName, effect, null, cardType);
        }

        return card;
    }

    public Card createCard(Card card, Player owner, Player target) {
        Card newCard = null;
        if (card.getType().equals("AttackCard")) {
            newCard = new AttackCard(card);
        } else if (card.getType().equals("HealCard")) {
            newCard = new HealCard(card);
        } else if (card.getType().equals("AttackBuffCard")) {
            newCard = new AttackBuffCard(card);
        }

        newCard.setOwner(owner);
        newCard.setTarget(target);

        return newCard;
    }
}
