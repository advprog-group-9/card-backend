package com.advprog.stratcard.cards.core;

import com.advprog.stratcard.players.core.Player;

public class AttackBuffCard extends Card {

    public AttackBuffCard(String cardName, String effect, Player owner, String type) {
        super(cardName, effect, owner, type);
    }

    public AttackBuffCard(Card card) {
        super(card);
    }

    @Override
    public void activateEffect() {
        if (this.effect == "Increase ATK by 15") {
            owner.addAtk(15);
        } else if (this.effect == "Increase ATK by 25") {
            owner.addAtk(25);
        } else if (this.effect == "Increase ATK by 5") {
            owner.addAtk(5);
        }
    }
}
