package com.advprog.stratcard.cards.core;

import com.advprog.stratcard.players.core.Player;

public class HealCard extends Card {

    public HealCard(String cardName, String effect, Player owner, String type) {
        super(cardName, effect, owner, type);
    }

    public HealCard(Card card) {
        super(card);
    }

    @Override
    public void activateEffect() {
        if (this.effect == "Heal user by 25 HP") {
            owner.addHp(25);
        } else if (this.effect == "Heal user by 50 HP") {
            owner.addHp(50);
        } else if (this.effect == "Heal user by 100 HP") {
            owner.addHp(100);
        }
    }
}
