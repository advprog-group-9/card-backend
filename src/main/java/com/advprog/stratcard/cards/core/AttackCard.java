package com.advprog.stratcard.cards.core;

import com.advprog.stratcard.players.core.Player;

public class AttackCard extends Card {

    public AttackCard(String cardName, String effect, Player owner, String type) {
        super(cardName, effect, owner, type);
    }

    public AttackCard(Card card) {
        super(card);
    }

    @Override
    public void activateEffect() {
        if (this.effect == "Do damage to enemy = atk") {
            target.addHp(-1 * owner.getAtk());
        } else if (this.effect == "Do damage to enemy = 2 * atk") {
            target.addHp(-2 * owner.getAtk());
        } else if (this.effect == "Do damage to enemy = 3 * atk") {
            target.addHp(-3 * owner.getAtk());
        }
    }
}
