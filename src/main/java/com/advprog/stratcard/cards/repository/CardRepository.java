package com.advprog.stratcard.cards.repository;

import com.advprog.stratcard.cards.core.Card;
import com.advprog.stratcard.cards.core.CardFactory;
import com.advprog.stratcard.players.core.Player;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

public class CardRepository {
    Map<Long, Card> cards = new HashMap<>();
    CardFactory cardFactory = new CardFactory();

    public Card getCard(long id) {
        Card trackedCard = cards.get(id);
        return trackedCard;
    }

    public void putCard(long cardId, Card card) {
        cards.putIfAbsent(cardId, card);
    }

    public int countNumOfCard() {
        return cards.size();
    }

    public Card[] randomAndGetFiveCards() {
        Object[] keys = cards.keySet().toArray();
        Random rand = new Random();
        Card[] randomCards = new Card[5];
        for (int keyNumber = 0; keyNumber < 5; keyNumber++) {
            int randomInt = rand.nextInt(keys.length - 1);
            Long randomLong = Long.parseLong(String.valueOf(randomInt));
            randomCards[keyNumber] = cards.get(randomLong);
        }

        return randomCards;
    }

    public void createCards() {
        int idStart = 0;
        putCard(idStart++, cardFactory.createCard("AttackCard", "Normal Attack ⚔", "Do damage to enemy = atk"));
        putCard(idStart++, cardFactory.createCard("AttackCard", "Strong Attack ⚔⚔", "Do damage to enemy = 2 * atk"));
        putCard(idStart++, cardFactory.createCard("AttackCard", "Critical Attack ⚔⚔⚔", "Do damage to enemy = 3 * atk"));
        putCard(idStart++, cardFactory.createCard("HealCard", "Small Heal +", "Heal user by 25 HP"));
        putCard(idStart++, cardFactory.createCard("HealCard", "Great Heal ++", "Heal user by 50 HP"));
        putCard(idStart++, cardFactory.createCard("HealCard", "Greater Heal +++", "Heal user by 100 HP"));
        putCard(idStart++, cardFactory.createCard("AttackBuffCard", "Weak Buff X", "Increase ATK by 5"));
        putCard(idStart++, cardFactory.createCard("AttackBuffCard", "Strong Buff XX", "Increase ATK by 15"));
        putCard(idStart++, cardFactory.createCard("AttackBuffCard", "Ultimate Buff XXX", "Increase ATK by 25"));
    }

    public Card copyCard(Card card, Player playerOne, Player playerTwo) {
        return cardFactory.createCard(card, playerOne, playerTwo);
    }

}
