package com.advprog.stratcard.ajaxresponse;

import com.advprog.stratcard.cards.core.Card;
import com.advprog.stratcard.players.core.Player;
import com.advprog.stratcard.room.core.Room;

public class AjaxResponse {
    String type;
    Card[] deck;
    Card[] playerOneCardUsed;
    Card[] playerTwoCardUsed;
    Player[] playerStats = new Player[2];


    public AjaxResponse(Room room) {
        playerStats[0] = room.getPlayerOne();
        playerStats[1] = room.getPlayerTwo();
    }

    public Player getPlayerOne() {
        return playerStats[0];
    }

    public Player getPlayerTwo() {
        return playerStats[1];
    }

    public Card[] getDeck() {
        return deck;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public void setDeck(Card[] deck) {
        this.deck = deck;
    }

    public void setPlayerOneCardUsed(Card[] cards) {
        playerOneCardUsed = cards;
    }

    public void setPlayerTwoCardUsed(Card[] cards) {
        playerTwoCardUsed = cards;
    }

    public Card[] getPlayerOneCardUsed() {
        return playerOneCardUsed;
    }

    public Card[] getPlayerTwoCardUsed() {
        return playerTwoCardUsed;
    }
}
