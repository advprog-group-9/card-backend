package com.advprog.stratcard.room.controller;

import com.advprog.stratcard.ajaxresponse.AjaxResponse;
import com.advprog.stratcard.cards.core.Card;
import com.advprog.stratcard.players.core.Player;
import com.advprog.stratcard.players.service.PlayerServiceImpl;
import com.advprog.stratcard.room.core.Room;
import com.advprog.stratcard.room.service.RoomServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import javax.validation.Valid;

@Controller
public class RoomController {

    @Autowired
    private RoomServiceImpl roomService;
    
    @Autowired
    PlayerServiceImpl playerService;

    @GetMapping("/")
    public String indexPage() {
        return "index";
    }

    @PostMapping("/")
    public String createRoom(@RequestParam(value = "username") String username, Model model) {
        Room room = roomService.createRoom();
        Player playerOne = new Player(username);
        Player playerTwo = new Player("Computer AI");
        room.setPlayerOne(playerOne);
        room.setPlayerTwo(playerTwo);

        model.addAttribute("currentRoom", room);
        return "room/board";
    }

    @GetMapping("/join/{tokenId}")
    public ResponseEntity<?> joinRoom(@PathVariable String tokenId, @RequestParam long playerId) {
        Room room = roomService.getRoomWithToken(tokenId);
        Player player = playerService.getPlayerWithId(playerId);
        room.setPlayerTwo(player);
        return ResponseEntity
            .status(HttpStatus.CREATED)
            .body(room.getToken());
    }

    @GetMapping("/register")
        public ResponseEntity<?> createRoom(@RequestParam long playerId, @RequestParam String username) {
        Player player = playerService.createPlayer(username, playerId);
        return ResponseEntity.ok(player);
    }

    @PostMapping("/move")
    public ResponseEntity<?> playerMove(@RequestParam long playerId, @RequestBody Card card) {
        Player player = playerService.getPlayerWithId(playerId);
        playerService.playerSelectAndUse(player, card);
        return ResponseEntity.ok("Card used");
    }

    @RequestMapping(path = "/start", method = RequestMethod.POST)
    public void startGame(@PathVariable String tokenId) {
        Room room = roomService.getRoomWithToken(tokenId);
        room.startGame();
    }
    @PostMapping("/get-players-stats")
    public @ResponseBody AjaxResponse getPlayerStats(@Valid @RequestBody String tokenId) {
        Room room = roomService.getRoomWithToken(tokenId);
        AjaxResponse response = new AjaxResponse(room);
        response.setType("GetPlayerStats");
        return response;
    }

    @PostMapping("/get-playerone-deck")
    public @ResponseBody AjaxResponse getPlayerOneDeck(@Valid @RequestBody String tokenId) {
        Room room = roomService.getRoomWithToken(tokenId);
        AjaxResponse response = new AjaxResponse(room);
        response.setType("getDeck");
        Card[] deck = roomService.randomAndGetFiveCards();
        Card[] playerDeck = new Card[5];
        Card[] computerDeck = new Card[3];

        for (int i = 0; i < deck.length; i++) {
            playerDeck[i] = roomService.copyCard(deck[i], room.getPlayerOne(), room.getPlayerTwo());
        }

        for (int i = 0; i < computerDeck.length; i++) {
            computerDeck[i] = roomService.copyCard(deck[i], room.getPlayerTwo(), room.getPlayerOne());
        }

        response.setDeck(playerDeck);
        room.getPlayerOne().setDeck(playerDeck);
        room.getPlayerTwo().setDeck(computerDeck);
        return response;
    }
    @PostMapping("/activate-selected-cards")
    public @ResponseBody AjaxResponse activateSelectedCards(@RequestBody String stringInput) {
        String[] splitted = stringInput.split(";");
        System.out.println(splitted[0]);
        Room room = roomService.getRoomWithToken(splitted[0]);
        room.getPlayerOne().activateSelectedCard(splitted);
        room.getPlayerTwo().activateSelectedCard();

        AjaxResponse response = new AjaxResponse(room);
        response.setType("ActivateCards");
        response.setPlayerOneCardUsed(room.getPlayerOne().getCardsUsed(splitted));
        response.setPlayerTwoCardUsed(room.getPlayerTwo().getDeck());
        return response;
    }


    @GetMapping(path = "/room/playing")
    public String getPlayingBoard(Model model) {
        return "room/board";
    }
}
