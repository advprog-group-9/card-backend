package com.advprog.stratcard.room.repository;

import com.advprog.stratcard.exception.RoomNotFoundException;
import com.advprog.stratcard.room.core.Room;
import com.advprog.stratcard.roomtokens.core.Token;
import org.springframework.stereotype.Repository;
import java.util.HashMap;
import java.util.Map;


@Repository
public class RoomRepository {
    private Map<String, Room> rooms = new HashMap<>();

    public Room putRoom(Token token, Room room) {
        Room existingRoom = rooms.get(token.getTokenId());
        if (existingRoom == null) {
            rooms.put(token.getTokenId(), room);
            return room;
        } else {
            return null;
        }
    }

    public Room getRoom(String tokenId) throws RoomNotFoundException {
        Room room = rooms.get(tokenId);
        return room;
    }
}
