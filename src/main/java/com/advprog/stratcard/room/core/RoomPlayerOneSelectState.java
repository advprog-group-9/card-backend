package com.advprog.stratcard.room.core;

import com.advprog.stratcard.cards.core.Card;
import com.advprog.stratcard.exception.MethodIsNotSupportedDuringThisStateException;
import com.advprog.stratcard.players.core.Player;

public class RoomPlayerOneSelectState implements RoomState {
    private Room room;
    Player player;

    public RoomPlayerOneSelectState(Room room) {
        this.room = room;

    }

    @Override
    public void playerOneSelectAndUse(Card card) {
        this.player = room.getPlayerOne();
        player.selectCard(card);
        player.useSelectedCard(room.getPlayerOne());
        room.setState(new RoomPlayerTwoSelectState(room));
    }

    @Override
    public void playerTwoSelectAndUse(Card card) {
        throw new MethodIsNotSupportedDuringThisStateException();
    }
}
