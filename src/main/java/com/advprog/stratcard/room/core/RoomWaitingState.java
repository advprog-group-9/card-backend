package com.advprog.stratcard.room.core;

import com.advprog.stratcard.cards.core.Card;
import com.advprog.stratcard.exception.MethodIsNotSupportedDuringThisStateException;

public class RoomWaitingState implements RoomState {
    private Room room;

    public RoomWaitingState(Room room) {
        this.room = room;
    }

    @Override
    public void playerTwoSelectAndUse(Card card) {
        throw new MethodIsNotSupportedDuringThisStateException();
    }

    @Override
    public void playerOneSelectAndUse(Card card) {
        throw new MethodIsNotSupportedDuringThisStateException();
    }

    
}
