package com.advprog.stratcard.room.core;

import com.advprog.stratcard.cards.core.Card;
import com.advprog.stratcard.exception.MethodIsNotSupportedDuringThisStateException;
import com.advprog.stratcard.players.core.Player;

public class RoomPlayerTwoSelectState implements RoomState {
    private Room room;
    private Player player;

    public RoomPlayerTwoSelectState(Room room) {
        this.room = room;

    }

    @Override
    public void playerTwoSelectAndUse(Card card) {
        this.player = room.getPlayerTwo();
        player.selectCard(card);
        player.useSelectedCard(room.getPlayerOne());
        room.setState(new RoomPlayerOneSelectState(room));
    }

    @Override
    public void playerOneSelectAndUse(Card card) {
        throw new MethodIsNotSupportedDuringThisStateException();
    }
}
