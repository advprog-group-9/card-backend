package com.advprog.stratcard.room.core;

import com.advprog.stratcard.cards.core.Card;

public interface RoomState {
    void playerOneSelectAndUse(Card card);
    void playerTwoSelectAndUse(Card card);
}
