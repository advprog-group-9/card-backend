package com.advprog.stratcard.room.core;

import com.advprog.stratcard.cards.core.Card;
import com.advprog.stratcard.players.core.Player;
import com.advprog.stratcard.roomtokens.core.Token;

public class Room {
    private Player playerOne;
    private Player playerTwo;
    private Token token;
    protected RoomState state;

    public Room(Token token) {
        state = new RoomWaitingState(this);
        this.token = token;
    }

    public void setPlayerOne(Player player) {
        playerOne = player;
    }

    public void setPlayerTwo(Player player) {
        playerTwo = player;
    }

    public void setState(RoomState state) {
        this.state = state;
    }

    public void startGame() {
        state = new RoomPlayerOneSelectState(this);
    }

    public void playerOneSelectAndUse(Card card) {
        state.playerOneSelectAndUse(card);
    }

    public void playerTwoSelectAndUse(Card card) {
        state.playerTwoSelectAndUse(card);
    }

    public Player getPlayerOne() {
        return playerOne;
    }

    public Player getPlayerTwo() {
        return playerTwo;
    }

    public Token getToken() {
        return token;
    }

    public boolean isPlayerOne(Player player) {
        return player == playerOne;
    }

    public RoomState getState() {
        return this.state;
    }

}
