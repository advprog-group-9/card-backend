package com.advprog.stratcard.room.service;

import com.advprog.stratcard.cards.core.Card;
import com.advprog.stratcard.cards.repository.CardRepository;
import com.advprog.stratcard.players.core.Player;
import com.advprog.stratcard.room.core.Room;
import com.advprog.stratcard.room.repository.RoomRepository;
import com.advprog.stratcard.roomtokens.core.Token;
import com.advprog.stratcard.roomtokens.core.TokenGenerator;
import org.springframework.stereotype.Service;

@Service
public class RoomServiceImpl implements RoomService {
    private final RoomRepository roomRepository;
    private final CardRepository cardRepository = new CardRepository();
    private final TokenGenerator tokenGenerator = new TokenGenerator();
    

    public RoomServiceImpl(RoomRepository roomRepository) {
        this.roomRepository = roomRepository;
        cardRepository.createCards();
    }

    @Override
    public Room createRoom() {
        Token token = tokenGenerator.generateToken();
        Room room = roomRepository.putRoom(token, new Room(token));
        return room;
    }

    @Override
    public Room getRoomWithToken(String tokenId) {
        return roomRepository.getRoom(tokenId);
    }

    @Override
    public Card[] randomAndGetFiveCards() {
        return cardRepository.randomAndGetFiveCards();
    }

    @Override
    public Card copyCard(Card card, Player playerOne, Player playerTwo) {
        return cardRepository.copyCard(card, playerOne, playerTwo);
    }

}
