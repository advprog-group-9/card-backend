package com.advprog.stratcard.room.service;

import com.advprog.stratcard.cards.core.Card;
import com.advprog.stratcard.exception.RoomNotFoundException;
import com.advprog.stratcard.players.core.Player;
import com.advprog.stratcard.room.core.Room;

public interface RoomService {
    Room createRoom();
    Room getRoomWithToken(String tokenId) throws RoomNotFoundException;
    Card[] randomAndGetFiveCards();
    Card copyCard(Card card, Player playerOne, Player playerTwo);
}
