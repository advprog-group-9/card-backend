package com.advprog.stratcard;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class StratcardApplication {
    public static void main(String[] args) {
        SpringApplication.run(StratcardApplication.class, args);
    }
}
