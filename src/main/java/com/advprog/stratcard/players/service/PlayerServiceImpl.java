package com.advprog.stratcard.players.service;

import com.advprog.stratcard.cards.core.Card;
import com.advprog.stratcard.players.core.Player;
import com.advprog.stratcard.players.repository.PlayerRepository;
import com.advprog.stratcard.room.core.Room;
import org.springframework.stereotype.Service;

@Service
public class PlayerServiceImpl implements PlayerService {
    private PlayerRepository playerRepository;

    public PlayerServiceImpl(PlayerRepository playerRepository) {
        this.playerRepository = playerRepository;
    }

    @Override
    public Player createPlayer(String username, long playerId) {
        Player player = new Player(username);
        playerRepository.putPlayer(playerId, player);
        return player;
    }

    @Override
    public Player getPlayerWithId(long playerId) {
        return playerRepository.getPlayer(playerId);
    }

    @Override
    public void playerSelectAndUse(Player player, Card card) {
        Room playersRoom = player.getRoom();
        if (playersRoom.isPlayerOne(player)) {
            playersRoom.playerOneSelectAndUse(card);
        } else {
            playersRoom.playerTwoSelectAndUse(card);
        }
    }
}
