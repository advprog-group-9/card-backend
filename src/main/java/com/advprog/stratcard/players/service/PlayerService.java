package com.advprog.stratcard.players.service;

import com.advprog.stratcard.cards.core.Card;
import com.advprog.stratcard.players.core.Player;

public interface PlayerService {
    Player createPlayer(String username, long playerId);
    Player getPlayerWithId(long id);
    void playerSelectAndUse(Player player, Card card);
}
