package com.advprog.stratcard.players.core;

import com.advprog.stratcard.cards.core.AttackCard;
import com.advprog.stratcard.cards.core.Card;
import com.advprog.stratcard.cards.core.HealCard;
import com.advprog.stratcard.room.core.Room;

public class Player {
    private String username;
    private int hp;
    private int atk;
    private Room room;
    private Card selectedCard;
    public Card[] deck;

    public Player(String username) {
        this.username = username;
        setDefaultAtk();
        setDefaultHp();
    }

    public void setRoom(Room room) {
        this.room = room;
    }

    public void setDefaultAtk() {
        atk = 10;
    }

    public void setDefaultHp() {
        hp = 250;
    }

    public int getHp() {
        return hp;
    }

    public int getAtk() {
        return atk;
    }

    public void addHp(int hp) {
        this.hp += hp;
    }

    public void addAtk(int atk) {
        this.atk += atk;
    }

    public Room getRoom() {
        return room;
    }

    public void selectCard(Card card) {
        this.selectedCard = card;
    }

    public void useSelectedCard(Player opponent) {
        if (selectedCard instanceof HealCard) {
            this.selectedCard.activateEffect();
        } else if (selectedCard instanceof AttackCard) {
            this.selectedCard.activateEffect();
        } else {
            this.selectedCard.activateEffect();
        }
        this.selectedCard = null;
    }

    public void activateCard(int index) {
        deck[index].activateEffect();
    }

    public void activateSelectedCard(String[] indexes) {
        for (int i = 1; i < indexes.length; i++) {
            this.activateCard(Integer.parseInt(indexes[i]));
        }
    }

    public void activateSelectedCard() {
        for (Card card : deck) {
            card.activateEffect();
        }
    }

    public String getUsername() {
        return username;
    }

    public void setDeck(Card[] deck) {
        this.deck = deck;
    }

    public Card[] getCardsUsed(String[] indexes) {
        Card[] cards = new Card[indexes.length - 1];
        for (int i = 1; i < indexes.length; i++) {
            cards[i - 1] = deck[Integer.parseInt(indexes[i])];
        }

        return cards;
    }

    public Card[] getDeck() {
        return deck;
    }
}
