package com.advprog.stratcard.players.repository;

import com.advprog.stratcard.players.core.Player;
import org.springframework.stereotype.Repository;
import java.util.HashMap;
import java.util.Map;

@Repository
public class PlayerRepository {
    Map<Long, Player> players = new HashMap<>();

    public Player getPlayer(long id) {
        return players.get(id);
    }

    public Player putPlayer(long playerId, Player player) {
        players.put(playerId, player);
        return player;
    }
}
