package com.advprog.stratcard.exception;

public class MethodIsNotSupportedDuringThisStateException extends RuntimeException {
    static final long serialVersionUID = 300;
}
