package com.advprog.stratcard.exception;

public class RoomNotFoundException extends RuntimeException {
    private static final long serialVersionUID = 404;
}
