package com.advprog.stratcard.roomtokens.core;

public class Token {
    private String tokenId;

    public Token(String tokenId) {
        setTokenId(tokenId);
    }

    public void setTokenId(String tokenId) {
        this.tokenId = tokenId;
    }

    public String getTokenId() {
        return tokenId;
    }

    public boolean isValid() {
        return tokenIdLengthIsFive() && tokenIdIsAlphanumeric();
    }

    private boolean tokenIdLengthIsFive() {
        return tokenId.length() == 5;
    }

    private boolean tokenIdIsAlphanumeric() {
        int tokenIdLength = 5;
        return checkTokenIdPerCharacter(tokenIdLength);
    }

    private boolean checkTokenIdPerCharacter(int tokenIdLength) {
        boolean validFlag = true;
        for (int i = 0; i < tokenIdLength; i++) {
            char currentChar = tokenId.charAt(i);
            if (!(Character.isDigit(currentChar) || Character.isLetter(currentChar))) {
                validFlag = false;
            }
        }

        return validFlag;
    }
}
