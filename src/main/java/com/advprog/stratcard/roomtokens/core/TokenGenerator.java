package com.advprog.stratcard.roomtokens.core;

import java.util.Random;

public class TokenGenerator implements TokenGeneratorInterface {
    @Override
    public Token generateToken() {
        Random digitOrCharRandom = new Random();
        StringBuilder tokenId = new StringBuilder();

        for (int i = 0; i < 5; i++) {
            if (digitOrCharRandom.nextInt(2) == 1) {
                tokenId.append((char) generateCharAscii());
            } else {
                tokenId.append((char) generateDigitAscii());
            }
        }

        return new Token(tokenId.toString());
    }

    @Override
    public int generateDigitAscii() {
        Random randomizer = new Random();
        return 48 + randomizer.nextInt(10);
    }

    @Override
    public int generateCharAscii() {
        Random randomizer = new Random();
        return 65 + randomizer.nextInt(26);
    }
}
