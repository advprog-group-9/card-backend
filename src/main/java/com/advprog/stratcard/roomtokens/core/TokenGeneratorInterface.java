package com.advprog.stratcard.roomtokens.core;

public interface TokenGeneratorInterface {
    Token generateToken();
    int generateDigitAscii();
    int generateCharAscii();
}
